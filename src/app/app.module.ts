import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ListFilmsComponent } from './films/list-films/list-films.component';
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import { FilmDetailsComponent } from './films/film-details/film-details.component';
import { FilmFormComponent } from './films/film-form/film-form.component';
import { NewFilmComponent } from './films/new-film/new-film.component';
import {ReactiveFormsModule} from "@angular/forms";
import { UpdateFilmComponent } from './films/update-film/update-film.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatSnackBar, MatSnackBarModule} from "@angular/material/snack-bar";
import { HeaderComponent } from './header/header.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatProgressBarModule} from "@angular/material/progress-bar";

@NgModule({
  declarations: [
    AppComponent,
    ListFilmsComponent,
    FilmDetailsComponent,
    FilmFormComponent,
    NewFilmComponent,
    UpdateFilmComponent,
    HeaderComponent,
  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot([
            {path: '', component: ListFilmsComponent},
            {path: 'films/new', component: NewFilmComponent},
            {path: 'films/:filmId', component: FilmDetailsComponent},
            {path: 'films/:filmId/update', component: UpdateFilmComponent}
        ]),
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatListModule,
        MatButtonModule,
        MatCardModule,
        MatInputModule,
        MatIconModule,
        MatSnackBarModule,
        MatToolbarModule,
        MatProgressBarModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
