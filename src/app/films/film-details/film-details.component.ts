import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Film} from "../interface/Film";
import {ActivatedRoute, Router} from "@angular/router";
import {FilmsService} from "../service/films.service";
import {map} from "rxjs/operators";
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.css']
})
export class FilmDetailsComponent implements OnInit {

  film!: Observable<Film | undefined>;
  idFilm = this.getIdFilm();

  constructor(private activatedRoute: ActivatedRoute,
              private filmsService: FilmsService,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.film = this.filmsService.getFilms()
      .pipe(map(films => films
        .find(film => film.id === this.idFilm)));

    this.film.subscribe(film => console.log(film));
  }

  getIdFilm() {
    const routeParams = this.activatedRoute.snapshot.paramMap;
    return Number(routeParams.get('filmId'));
  }

  onDelete() {
    this.snackBar.open('Film supprimé', 'Fermer');
    this.filmsService.deleteFilm(this.idFilm)
      .subscribe(() => this.router.navigate(['']));
  }

}
