import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Film} from "../interface/Film";
import {FilmsService} from "../service/films.service";
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';
import {Router} from "@angular/router";
import {MatProgressBar} from "@angular/material/progress-bar";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-list-films',
  templateUrl: './list-films.component.html',
  styleUrls: ['./list-films.component.css']
})
export class ListFilmsComponent implements OnInit {

  films: Observable<Film[]> | undefined;
  isLoading = true;


  constructor(private filmsService: FilmsService,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.films = this.filmsService.getFilms().pipe(tap(() => {
      this.isLoading= false;
    }));
  }

  onDelete(id: number) {
    this.snackBar.open('Film supprimé', 'Fermer');
    this.filmsService.deleteFilm(id)
      .subscribe();
    window.location.reload();
  }

  onFav(film: any) {
    if (film.isFav) {
      this.snackBar.open('Retiré des favoris', 'Fermer');
      film.isFav = false;
    } else {
      this.snackBar.open('Ajouté aux favoris', 'Fermer');
      film.isFav = true;
    }
    this.filmsService.updateFilm(film).subscribe();
  }

}
