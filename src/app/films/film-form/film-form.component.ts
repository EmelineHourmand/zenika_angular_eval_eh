import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FilmsService} from "../service/films.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {Film} from "../interface/Film";
import {map} from "rxjs/operators";
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';

@Component({
  selector: 'app-film-form',
  templateUrl: './film-form.component.html',
  styleUrls: ['./film-form.component.css']
})
export class FilmFormComponent implements OnInit {
  checkoutForm!: FormGroup;
  submitted = false;

  film!: Observable<Film | undefined>;
  idFilm = this.getIdFilm();

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private filmsService: FilmsService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar) { }

  creatForm() {
    this.checkoutForm = this.formBuilder.group({
      id: '',
      title: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(75)]],
      synopsis: ['', Validators.required],
      releaseYear: ['', [Validators.required, Validators.pattern('^(19|20)\\d{2}$')]],
      director: ['', Validators.required],
      imbdRating: ['', [Validators.required, Validators.pattern('(^[0-9](.[0-9])?$)|10')]],
      isFav: false
    });
  }

  get f() { return this.checkoutForm.controls; }

  onSubmit(): void {

    this.submitted = true;

    if (this.checkoutForm.invalid) {
      return;
    }

    if (this.idFilm !== 0) {
      this.snackBar.open('Film mis à jour', 'Fermer');
      this.filmsService.updateFilm(this.checkoutForm.value)
        .subscribe(() => this.router.navigate(['']));
    } else {
      this.snackBar.open('Le fim à été ajouté', 'Fermer');
      this.filmsService.addFilm(this.checkoutForm.value)
        .subscribe(() => this.router.navigate(['']));
    }
  }

  ngOnInit(): void {
    this.creatForm();

    if (this.idFilm !== undefined ) {
      console.log('upatde');
      this.film = this.filmsService.getFilms()
        .pipe(map(films => films.find(film => film.id === this.idFilm)));

      this.film.subscribe(res => this.checkoutForm.patchValue(res || {}));
    }
  }

  getIdFilm() {
    const routeParams = this.activatedRoute.snapshot.paramMap;
    return Number(routeParams.get('filmId'));
  }

}
