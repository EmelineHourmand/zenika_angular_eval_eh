import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Film} from "../interface/Film";

const filmsUrl = environment.api + "/films";

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(private http: HttpClient) { }

  getFilms() {
    return this.http.get<Film[]>(filmsUrl);
  }

  addFilm(value: any) {
    return this.http.post(filmsUrl, value);
  }

  updateFilm(value: any) {
    console.log(value);
    return this.http.put(filmsUrl + '/' + value.id, value);
  }

  deleteFilm(id: number) {
    return this.http.delete(filmsUrl + '/' + id);
  }
}
