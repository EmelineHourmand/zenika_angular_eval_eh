export interface Film {
  id: number;
  title: string;
  synopsis: string;
  releaseYear: number;
  director: string;
  imbdRating: number;
  isFav: false;
}
