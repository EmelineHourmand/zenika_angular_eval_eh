/// <reference types="cypress" />

describe("Eval", () => {
  describe("Home page", () => {
    it("list the films", () => {
      mockList();

      cy.visit("http://localhost:4200");
      // Title
      cy.contains("Hyper sympa toch");
      // director
      cy.contains("by Foo Bar");
      // imbdRating
      cy.contains("(2.3)");

      // Title
      cy.contains("Angular is the best ?");
      // director
      cy.contains("by Misko Hevery");
      // imbdRating
      cy.contains("(5.5)");
    });

    it("go to detail", () => {
      mockList();
      mockOne();

      cy.visit("http://localhost:4200");

      cy.contains("Hyper sympa toch").click();

      cy.location().should((l) => {
        expect(l.href).to.have.string("/films/1");
      });
    });

    it("detail display synopsis, ...", () => {
      mockList();
      mockOne();

      cy.visit("http://localhost:4200");

      cy.contains("Hyper sympa toch").click();

      cy.contains("Il était une fois, un type ultra ...");
    });

    it("create a film btn", () => {
      mockList();
      cy.visit("http://localhost:4200");
      cy.contains("New film").click();

      cy.location().should((l) => {
        expect(l.href).to.have.string("/films/new");
      });
    });

    it("create a film", () => {
      mockList();
      cy.intercept("POST", "/films").as("createFilm");
      cy.visit("http://localhost:4200");
      cy.contains("New film").click();

      // Il faudra mettre les Ids dans les inputs !
      cy.get("#title").type("new title");
      cy.get("#synopsis").type("my synop");
      cy.get("#releaseYear").type("2021");
      cy.get("#director").type("Me");
      cy.get("#imbdRating").type("7.8");
      cy.get("form").submit();

      cy.wait("@createFilm")
    });

    it("update a film", () => {
      mockList();
      mockOne();
      cy.intercept("PUT", "/films/1").as("updateFilm");

      cy.visit("http://localhost:4200");

      cy.contains("Hyper sympa toch").click();

      cy.contains("Update film").click();

      cy.get("#title").should('have.value', 'Hyper sympa toch')

      // Il faudra mettre les Ids dans les inputs !
      cy.get("#title").clear().type("New title");
      cy.get("#synopsis").clear().type("my synop");
      cy.get("#releaseYear").clear().type("2021");
      cy.get("#director").clear().type("Me");
      cy.get("#imbdRating").clear().type("7.8");
      cy.get("form").submit();

      cy.wait("@updateFilm");
    });

    it ("delete film", () => {
      mockList();
      mockOne();

      cy.intercept("DELETE", "/films/1").as("deleteFilm");

      cy.visit("http://localhost:4200");

      cy.contains("Hyper sympa toch").click();

      cy.contains("Delete film").click();

      cy.wait("@deleteFilm");

    });

    it ("add to fav", () => {
      mockList();
      mockOne();

      cy.intercept("PUT", "/films/1").as("addFav");
      cy.visit("http://localhost:4200");

      cy.contains("Fav").click();

      cy.wait('@addFav');

    })


  });
});

function mockList() {
  cy.intercept("GET", "/films", [
    {
      id: 1,
      title: "Hyper sympa toch",
      synopsis: "Il était une fois, un type ultra ...",
      releaseYear: 2018,
      director: "Foo Bar",
      imbdRating: 2.3,
    },
    {
      id: 2,
      title: "Angular is the best ?",
      synopsis: "Une guerre sans merci ...",
      releaseYear: 2020,
      director: "Misko Hevery",
      imbdRating: 5.5,
    },
  ]);
}

function mockOne() {
  cy.intercept("GET", "/films/1", {
    id: 1,
    title: "Hyper sympa toch",
    synopsis: "Il était une fois, un type ultra ...",
    releaseYear: 2018,
    director: "Foo Bar",
    imbdRating: 2.3,
  }).as("getFilm");
}
